import React, { PropTypes } from 'react';
import styles from './SongItem.css';

const SongItem = (props) => {
  const { songData } = props;
  return (
    <div className={styles.root}>
      <div className={styles.album}>
        <img role="presentation" src={songData.album.images[1].url} />
        <span className={styles.albumName}>{songData.album.name}</span>
      </div>
      <div className={styles.songAndDescription}>
        <audio controls duration src={songData.preview_url} />
        <span className={styles.songDescription}>Name: {songData.name}</span>
        <span className={styles.songDescription}>Duration: {songData.duration_ms / 1000} s</span>
      </div>
    </div>
  );
};

SongItem.propTypes = {
  songData: PropTypes.object,
}

export default SongItem;
