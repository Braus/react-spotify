import React, { PropTypes } from 'react';
import styles from './SongList.css';

const eachSong = (props) => {
  const { album, name } = props;
  const albumImage = album.images[0];
  return (
    <div className={styles.eachSong}>
      <img role="presentation" src={albumImage.url} className={styles.image} />
      <span className={styles.songDescription}>{name}</span>
    </div>
  );
};

eachSong.proptypes = {
  album: PropTypes.array,
  name: PropTypes.string,
};

const SongList = (props) => (
  <div className={styles.root}>
    {props.listOfSongs.items.map((songData, index) => (
      <div key={songData.uri} onClick={() => props.selectSong(index)}>
        {eachSong(songData)}
      </div>
    ))}
  </div>
);

SongList.proptypes = {
  listOfSongs: PropTypes.array,
  selectSong: PropTypes.func,
};

export default SongList;
