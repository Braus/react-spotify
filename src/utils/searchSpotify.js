import axios from 'axios';
import querystring from 'query-string';

const clientId = 'd4cbda3bb29443468f2694c2d0a50c6e';
const clientSecret = 'ba02b036d779401b9aa2fb9eaabe8d8f';

const searchSpotify = (searchItem) => new Promise((resolve, reject) => {
  const authOptions = {
    method: 'POST',
    url: 'https://accounts.spotify.com/api/token',
    data: querystring.stringify({
      grant_type: 'client_credentials',
    }),
    headers: {
      'Authorization': 'Basic ' + (new Buffer(clientId + ':' + clientSecret).toString('base64')),
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    json: true,
  };
  axios(authOptions).then((res) => {
    axios.get(`https://api.spotify.com/v1/search?q=${searchItem}&type=track`, {
      headers: { 'Authorization': 'Bearer ' + res.data.access_token },
    }).then(({ data }) => resolve(data))
      .catch((err) => reject(err));
  }).catch((err) => (err));
});

export default searchSpotify;
